# OACP1-Protocols
This repository contains all lab reports of the ETHZ OACP1 lab course. 

Note: If you take them as a template for content, please be aware of the fact that I also had to improve myself. This is why later reports are generally better than previous ones.

## The Experiments I had

- 1c
- 2b
- 3b
- 4b
- 5b
- 6b
- 7b
- 8c
- 9c

## Reproduction of the documents

If you want to reproduce the layout, you have to know the following things:

- All reports were written using the luaLaTeX (Distribution: TeX live for Arch Linux) compiler and bibtex for bibliography entries. It should be enough to have the TeX live medium pack installed in order to use all the features. 
- The .tex files won't compile, as they depend on my template files (preamble.tex, lstsetup.tex, chemicalMakros.tex). These can be found using [this link](https://github.com/alexander-schoch/templates). In order to use them, it is necessary to download them and change the absolute paths in the .tex files.
- The template file `preamble.tex` depends on the `CormorantGaramond-Light.ttf` font. Additionally, the bibliography needs the very similar `CormorantGaramond.ttf` font, because there are some very weird alignment problems when using the Light font. If those fonts are not installed, the compilation will fail. 
- The H & P statements were generated using [this self-made software](https://gitlab.com/alexander-schoch/hazardstatements)
- If you have any questions, you can contact me directly on GitHub.
